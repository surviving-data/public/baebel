# Robot T :: Translate automation

## Usage with compose

```yaml
robot-t:
    image: survivingdata/robot-t
    volumes:
        - ./locales/:/locales
        - ./project1/src:/sources/nodejs/project1
        - ./project2/src:/sources/nodejs/project2
        - ./project3/py:/sources/python/project3
    restart: unless-stopped
```
