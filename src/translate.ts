const util = require('util')
const execSync = require('child_process').execSync
const gettextParser = require('gettext-parser')
const fs = require('fs')

const { mergeNewMessages } = require('./mergeNewMessages')
const { translateRandomEmpty } = require('./translateRandomEmpty')
const { randFloat, sample } = require('./utils')
const { waitThresholdDefault } = require('./variables')

let waitThreshold = waitThresholdDefault

const translate = async () => {
    // Make the first sourcing analyze to make POT file
    await mergeNewMessages()

    // While true
    while (true) {
        // 20% of the time, make the first sourcing analyze again
        randFloat(1, 5) == 5 && (await mergeNewMessages())

        // Random translation
        const translated = await translateRandomEmpty()

        // Sleep randomly
        const waitingTime = translated ? randFloat(waitThreshold, waitThreshold + 10000) : 3000
        console.log(`Waiting for ${waitingTime} ms`)
        await new Promise((resolve) => setTimeout(resolve, waitingTime))
    }
}

translate()
