export const localesPath = '/locales'
export const sourcesPath = '/sources'

export const waitThresholdDefault = 5000
export const waitThresholdAfterBannishment = 10000
