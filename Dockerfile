FROM node:lts
ENV NODE_ENV=development
ARG DEV_MODE=0
RUN apt update
RUN apt install -y gettext
WORKDIR /app
RUN npm install -g nodemon --save-dev
COPY ./ ./
ENTRYPOINT [ "/bin/bash", "/app/entrypoint" ]
CMD ["trans"]